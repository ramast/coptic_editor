# Coptic Editor

This project allow for writing in Coptic alphabet.
It can also configured to write in other extinct alphabets.

## License
Project is released under the GPLv2 license.

## Demo
Editor is being used here https://editor.danacbe.com/

## How does it work?
The core of the editor is a table (called Character Map).
The editor will actively replace text found on the left column with corresponding text from the right column.

## Customization
The editor has two levels of customization.

### Basic Customization:
* Click on Customize Alphabet table then define which letter 
  in English alphabet (or any other alphabet) should be replaced with which Coptic letter.
* Changes apply instantly and are automatically saved in your web browser
* Click "Reset to defaults" if you want to discard all the changes you've made
* Click "Export table" to save your table as CSV file.
* Click "Import table" to load table you have previously exported.

### Advanced Customization:
1. Click on "Export table" to export current table as CSV file
2. Open the file using "Microsoft Excel" or "Libreoffice Calc" or use https://edit-csv.net/
3. Make sure that "Delimiter" is set to `comma`
4. Alter both columns as you desire.
5. Go to the Editor and import the new csv file

### Additional writing systems:
By default, editor work as Coptic editor however other writing systems can be supported.
At the moment only Coptic and Hieroglyphic are supported.
Coptic can be accessed by passing the parameter `/?ws=coptic` in the url
while for Hieroglyphic it's `/?ws=hieroglyphic`.

See `public/writing_systems` directory in this repo to view supported writing systems or to add new one.

## Embedding Editor into different website
Editor can be embedded into other websites either as an iframe or popup.
Editor can communicate with main window through messages.

### Editor's url parameters

| Parameter           | default value                | Description                                                                   |
|---------------------|------------------------------|-------------------------------------------------------------------------------|
| ws                  | coptic                       | Writing System the editor will translate to                                   |
| embed               | (unset)                      | Minimal Editor UI suitable for embedding                                      |
| saveButton          | (unset)                      | Add save button. Value of parameter will be button's text                     |
| initialText         | (Emptry string)              | Initial / default text inside the textbox                                     |
| placeholder         | "You can write in {ws} here" | Set textbox placeholder                                                       |
| fixedRows           | (unset)                      | If set, textbox's rows will not change. Additional lines introduces scrollbar |
| rows                | 3                            | initial number of rows of the textbox                                         |
| expandAlphabetTable | (unset)                      | if set, it will force alphabet table to be expanded                           |
| readonly            | (unset)                      | if set, it will force textbox to be readonly                                  |
| caretEvent          | (unset)                      | if set, a message is sent when textbox cursor position changes                |
| changeEvent         | (unset)                      | if set, a message is sent when textbox's content changes                      |
| blurEvent           | (unset)                      | if set, a message is sent when textbox loses focus                            |

### Editor's Events

* **init**: Editor has been initialized
* **closed**: Editor has been closed
* **textChanged**: Editor's text has changed (only if `textChangeEvent` parameter is set to 1)
* **caretPositionChanged**: Editor's caret [blinking cursor] position has changed (only if `caretPositionEvent` is set to 1).
* **blur**: Editor's textbox has lost focus (only if `blurEvent`) is set to 1).
* **save**: Save button - if enabled - has been clicked

It's possible to see messages sent from the Editor by opening developer console and enable the debug messages.

### Editor's Functions
* **setText**: Set Editor's textbox content (string).
* **setAlphabetTable**: Define a custom alphabet table (array). Example: `[["a": "ⲁ"], ["b": "ⲃ"]]`
* **setPosition**: Set cursor position of the textbox (number).
* **setReadOnly**: Set textbox to be readonly (true/false)

### Example:
Some code examples can be found in this repo under `examples/embed/`

# Development and Installation
## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)
